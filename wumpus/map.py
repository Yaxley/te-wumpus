from const import *

import pygame
from pygame.locals import *

import random

EAST = 0
SOUTH = 1
WEST = 2
NORTH = 3

DIRECTIONS = ((1, 0), (0, 1), (-1, 0), (0, -1))

WALL   = 0
FLOOR  = 1
PIT    = 2
GOLD   = 3
WUMPUS = 4
AGENT  = 5

OPEN = (FLOOR, GOLD, WUMPUS)

FOG  = 0
SEEN = 1

COLORS = {WALL:   (100, 100, 100),
          FLOOR:  (200, 200, 200),
          GOLD:   (255, 174,  13),
          PIT:    (  0,   0,   0),
          WUMPUS: (  0, 255,   0),
          AGENT:  (108,  70, 255)}

VALID_ACTIONS = (WALK, ROTATE_LEFT, ROTATE_RIGHT, FIRE, CLIMB, LOOT)

class Dungeon():
    def __init__(self, screen, font, size, square_size, **kwargs):
        self.screen = screen
        self.font = font
        self.font_large = pygame.font.SysFont('monospace', 18)
        self.size = size
        self.sq_size = square_size
        self.points = 0
        self.total_moves = 0
        self.agent_alive = True
        self.agent_exited = False
        self.wumpus_alive = True
        self.gold_found = False
        self.bump = False
        self.scream = False

        self.agent_direction = EAST

        self.square_map = kwargs['square'] if 'square' in kwargs else False
        self.num_pits =  kwargs['pits']  if 'pits' in kwargs else 10
        self.irregular_size = kwargs['irregular_size'] if 'irregular_size' in kwargs else 100


        self.map, self.agent, self.gold, self.wumpus = self.generate_map()
        while not Dungeon.verify_map(self.map, self.agent, self.gold):
            self.map, self.agent, self.gold, self.wumpus = self.generate_map()

        self.start = self.agent # Store start position for later

        self.fow = [[FOG] * size for i in [FOG] * size]
        self.fow[self.start[0]][self.start[1]] = SEEN

    def generate_map(self):
        #print "Generating map."
        map = [[WALL] * self.size for i in range(self.size)]
        if self.square_map:
            # Square map
            #print "Square map selected."
            for i in range(1, self.size - 1):
                for j in range(1, self.size - 1):
                    map[i][j] = FLOOR
            start = (random.randint(1, self.size - 2), random.randint(1, self.size - 2))
        else:
            # Non-uniform map
            #print "Non-uniform map selected."
            start = (random.randint(2, self.size - 3), random.randint(2, self.size - 3))
            x, y = start
            while Dungeon.count_open(map) < self.irregular_size:
                direction = random.choice(DIRECTIONS)
                x, y = x + direction[0], y + direction[1]

                if x > self.size - 3: x = self.size - 3
                if x < 2: x = 2
                if y > self.size - 3: y = self.size - 3
                if y < 2: y = 2

                for i, j in ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 0), (0, 1), (1, -1), (1, 0), (1, 1)):
                    map[x + i][y + j] = FLOOR

        #print "Adding pits."
        interval = self.num_pits / 10
        num_pits = self.num_pits + random.randint(-interval, interval)
        while num_pits > 0:
            x, y = random.randint(1, self.size - 2), random.randint(1, self.size - 2)
            if map[x][y] in OPEN and not (x, y) == start:
                map[x][y] = PIT
                num_pits -= 1
        
        #print "Placing gold."
        gold = None
        while not gold:
            x, y = random.randint(1, self.size - 2), random.randint(1, self.size - 2)
            if map[x][y] in OPEN and not (x, y) == start:
                gold = (x, y)

        #print "Spawning Wumpus."
        wumpus = None
        while not wumpus:
            x, y = random.randint(1, self.size - 2), random.randint(1, self.size - 2)
            if map[x][y] in OPEN and not (x, y) == start and not (x, y) == gold:
                wumpus = (x, y)

        return map, start, gold, wumpus

    @staticmethod
    def count_open(map):
        count = 0
        for list in map:
            for item in list:
                if item in OPEN:
                    count += 1
        return count

    @staticmethod
    def verify_map(map, start, gold):
        #print "Verifying map."
        visited = []
        queue = [start]
        while queue:
            x, y = queue.pop(0)
            if map[x][y] in OPEN and not (x, y) in visited:
                if (x, y) == gold:
                    return True
                visited.append((x, y))
                for dx, dy in DIRECTIONS:
                    queue.append((x + dx, y + dy))
        return False

    def distance_map(self, x, y):
        map = [[-1] * self.size for i in range(self.size)]
        max = 0
        queue = [(x, y, 0)]
        while queue:
            i, j, d = queue.pop(0)
            if self.map[i][j] in OPEN and map[i][j] == -1:
                map[i][j] = d
                if d > max: max = d
                for i_d, j_d in DIRECTIONS:
                    queue.append((i + i_d, j + j_d, d + 1))

        self.dist_map = map

    def draw(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.fow[i][j] is FOG:
                    color = [(c + 50) / 2 for c in COLORS[self.map[i][j]]]
                else:
                    color = COLORS[self.map[i][j]]
                pygame.draw.rect(self.screen, color, (i * self.sq_size, j * self.sq_size, self.sq_size, self.sq_size))
                
        pygame.draw.rect(self.screen, [c / 2 for c in COLORS[WUMPUS]], (self.wumpus[0] * self.sq_size + 2, self.wumpus[1] * self.sq_size + 2, self.sq_size - 4, self.sq_size - 4))
        text = self.font_large.render("W", 1, COLORS[WUMPUS])
        self.screen.blit(text, (self.wumpus[0] * self.sq_size + self.sq_size / 2 - text.get_width() / 2 - 1, self.wumpus[1] * self.sq_size + self.sq_size / 2 - text.get_height() / 2))

        pygame.draw.rect(self.screen, [c / 2 for c in COLORS[AGENT]], (self.start[0] * self.sq_size + 1, self.start[1] * self.sq_size + 1, self.sq_size - 2, self.sq_size - 2), 1)

        if self.agent_alive:
            pygame.draw.rect(self.screen, [c / 2 for c in COLORS[AGENT]], (self.agent[0] * self.sq_size + 2, self.agent[1] * self.sq_size + 2, self.sq_size - 4, self.sq_size - 4))
        text = self.font_large.render("A", 1, COLORS[AGENT])
        text = pygame.transform.rotate(text, (self.agent_direction + 1) % 4 * -90)
        self.screen.blit(text, (self.agent[0] * self.sq_size + self.sq_size / 2 - text.get_width() / 2, self.agent[1] * self.sq_size + self.sq_size / 2 - text.get_height() / 2))
                    
        if not self.gold_found:
            pygame.draw.rect(self.screen, [c / 2 for c in COLORS[GOLD]], (self.gold[0] * self.sq_size + 2, self.gold[1] * self.sq_size + 2, self.sq_size - 4, self.sq_size - 4))
            text = self.font_large.render("G", 1, COLORS[GOLD])
            self.screen.blit(text, (self.gold[0] * self.sq_size + self.sq_size / 2 - text.get_width() / 2 - 1, self.gold[1] * self.sq_size + self.sq_size / 2 - text.get_height() / 2))


    def get_stimuli(self):
        if not self.agent_alive and not self.agent_exited:
            return Exception('DEAD')

        x, y = self.agent

        glitter = True if (x, y) == self.gold and not self.gold_found else False

        breeze = False
        for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1), (0, 0)):
            if self.map[x + dx][y + dy] == PIT:
                breeze = True

        stench = False
        for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1), (0, 0)):
            if (x + dx, y + dy) == self.wumpus:
                stench = True

        bump = self.bump
        if self.bump: self.bump = False

        scream = self.scream
        if self.scream: self.scream = False

        return glitter, stench, breeze, bump, scream

    def make_move(self, action):
        if not self.agent_alive:
            return Exception('DEAD')

        if self.agent_exited:
            return Exception('Elvis has left the building')

        if not action in VALID_ACTIONS:
            raise ValueError

        self.total_moves += 1

        if action == WALK:
            self.points -= 1
            x, y = self.agent
            dx, dy = DIRECTIONS[self.agent_direction]
            if self.map[x + dx][y + dy] in OPEN:
                self.agent = x + dx, y + dy
                self.fow[x + dx][y + dy] = SEEN # Make sure to change Fog of War
            elif self.map[x + dx][y + dy] == PIT:
                self.agent = x + dx, y + dy
                self.agent_alive = False
                self.points -= 1000
            else:
                self.bump = True

            if self.wumpus_alive and self.agent == self.wumpus:
                self.agent_alive = False
                self.points -= 1000

        elif action == ROTATE_LEFT:
            self.agent_direction = (self.agent_direction - 1) % 4

        elif action == ROTATE_RIGHT:
            self.agent_direction = (self.agent_direction + 1) % 4

        elif action == FIRE:
            self.points -= 1
            x, y = self.agent
            while not self.map[x][y] == WALL:
                if (x, y) == self.wumpus:
                    self.wumpus_alive = False
                    self.scream = True
                    self.points += 100
                x, y = x + DIRECTIONS[self.agent_direction][0], y + DIRECTIONS[self.agent_direction][1]

        elif action == CLIMB:
            self.points -= 1
            if self.agent == self.start and self.gold_found:
                self.agent_exited = True
                self.points += 1000

        elif action == LOOT:
            self.points -= 1
            if self.agent == self.gold:
               self.gold_found = True
               self.points += 100